<br>
這個專案有兩個主要的分支：  

- NormalHashTable: 一般的 Hash Table ，使用Linked List 來解決 Hash Collision 的問題。
- pthreadHashTable: 使用 pthread 來替網路上找的Cuckoo追加查找與刪除的平行化功能。

### NormalHashTable
此資料夾共有六種不同的實做：  
 
- 互斥鎖加上大鎖（全域鎖）
- 互斥鎖加上條紋鎖 （輪替四把鎖）
- 互斥鎖加上桶鎖（一個桶子一把鎖）
- 讀者寫者鎖加上大鎖（全域鎖）
- 讀者寫者鎖加上條紋鎖 （輪替四把鎖）
- 讀者寫者鎖加上桶鎖（一個桶子一把鎖）

這些程式都擁有一樣的介面，藉由`perf-normal-hashtable.cpp`測量運行時間評估效能。  
編譯的指令如下，這樣可以輸出一個執行檔。  
`g++ -O3 -fopenmp -std=c++17  -o test.o pref-normal-hashtable.cpp *.hpp`

### pthreadHashTable
這個資料夾存放我們修改[cuckoo-hashing-CUDA](https://github.com/josehu07/cuckoo-hashing-CUDA)開源專案單執行緒版本的程式，我們使用Pthread嘗試將其改為可以平行化的版本。  
本資料夾有寫makefile，所以只要下`make`就可以編譯出執行檔。